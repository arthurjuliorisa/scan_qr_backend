const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports = {
    generateAccessToken: (list) => {
        return jwt.sign(JSON.stringify(list), process.env.ACCESS_TOKEN_SECRET);
    },
    tokenVerify:(token)=>{
        return jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    }
}
