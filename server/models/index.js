const pool = require("../config/database");

let kangsendb = {};


kangsendb.all = () =>{
    return new Promise((resolve, reject)=>{
        pool.query("select * from mst_members", (err, results)=>{
            if(err){
                return reject(err);
            }
            return resolve(results);
        });
    });
}

kangsendb.one = (id) =>{
    return new Promise((resolve, reject)=>{
        pool.query("select * from mst_members where id_member = ?", [id], (err, results)=>{
            if(err){
                return reject(err);
            }
            return resolve(results);
        });
    });
}

module.exports = kangsendb;