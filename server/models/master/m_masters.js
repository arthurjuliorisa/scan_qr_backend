const db = require("../../config/database");

let master = {};

master.all = (page, perpage) => {
  page = perpage * page;
  const sql =
    "SELECT user, nik, nama, substr(waktu, 1, 10) waktu \
    from mst_user \
    LIMIT " +
    perpage +
    " OFFSET " +
    page;

  return new Promise((resolve, reject) => {
    db.query(sql, (err, results) => {
      if (err) {
        return reject(err);
      } else {
        return resolve(results);
      }
    });
  });
};

master.one = (id) => {
  const sql =
    "SELECT user, nik, nama, substr(waktu, 1, 10) waktu \
    from mst_user \
    where id='" +
    id +
    "'";

  return new Promise((resolve, reject) => {
    db.query(sql, (err, results) => {
      if (err) {
        return reject(err);
      } else {
        return resolve(results);
      }
    });
  });
};

module.exports = master;
