const db = require("../../config/database");

let member = {};


member.all = (page, perpage) => {
    page = perpage * page;
    const sql = "SELECT mm.id_member, mm.full_name, mm.phone_number, mm.identity_number, \
    mm.username, lv.village_name, ld.district_name, lr.name as regency_name, \
    lp.name as province_name \
    from mst_members mm \
    LEFT JOIN loc_villages lv ON lv.id_village = mm.village \
    LEFT JOIN loc_districts ld ON lv.id_district =  ld.id_district \
    LEFT JOIN loc_regencies lr ON ld.id_regency = lr.id_regency \
    LEFT JOIN loc_provinces lp ON lr.id_province = lp.id_province \
    LIMIT "+ perpage + " OFFSET " + page;
    

    return new Promise((resolve, reject) => {
        db.query(sql, (err, results) => {
            if (err) {
                return reject(err);
            }
            else{
                return resolve(results);
            }
        });
    });
}

member.one = (id) => {
    return new Promise((resolve, reject) => {
        db.query("select * from mst_members where id_member = ?", [id], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolve(results);
        });
    });
}

module.exports = member;