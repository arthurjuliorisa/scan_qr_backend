const router = require("express").Router();
m_auth = require("../models/m_auth");

router.post("/login/member", async (req, res, next) => {
  try {
    let results = await m_auth.loginmember(
      req.body.username,
      req.body.password
    );
    if (results != 401 || results != 403) {
      res.json(results);
    } else {
      res.status(parseInt(results));
      res.send("Unauthorized!");
    }
  } catch (e) {
    console.log(e);
    res.status(500).send(e);
  }
});

router.get("/", async (req, res) => {
  try {
    const token = req.headers["authorization"];
    res.json(tokenizer.tokenVerify(token.split(" ")[1]));
  } catch (e) {
    console.log(e);
    res.sendStatus(500);
  }
});

router.post("/login/user", async (req, res) => {
  //   console.log(req.body.username);
  //   console.log(req.body.body);
  try {
    let results = await m_auth.login_user(req.body.username, req.body.password);
    if (results != 401 || results != 403) {
      res.json(results);
    } else {
      res.status(parseInt(results));
      res.send("Unauthorized!");
    }
  } catch (e) {
    console.log(e);
    res.sendStatus(500);
  }
});

module.exports = router;
// iki test git
