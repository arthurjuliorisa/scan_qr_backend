const express = require("express");
// const member = ;
const router = express.Router();

router.use("/members", require("./master/members"));
router.use("/auth", require("./auth"));
router.use("/masters", require("./master/masters"));
router.use("/masters/one", require("./master/masters"));

module.exports = router;
