const express = require("express");
const router = express.Router();
const master = require("../../models/master/m_masters");

router.post("/", async (req, res) => {
  try {
    const data = req.body;
    const results = await master.all(data["page"], data["perpage"]);
    res.json(results);
  } catch (e) {
    console.log(e);
    res.sendStatus(500);
  }
});

router.post("/one", async (req, res) => {
  try {
    const data = req.body;
    const results = await master.one(data["id"]);
    res.json(results);
  } catch (e) {
    console.log(e);
    res.sendStatus(500);
  }
});

module.exports = router;
