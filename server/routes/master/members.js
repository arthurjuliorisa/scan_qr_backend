const express = require("express");
const router = express.Router();
const member = require("../../models/master/m_members");
const tokenizer = require("../../helpers/tokenizer");

router.post('/', async (req, res) => {
    try {
        const token = req.headers['authorization'];
        const data = req.body;
        tokenizer.tokenVerify(token.split(" ")[1]);
        const results = await member.all(data['page'], data['perpage']);
        res.json(results);
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

module.exports = router;