const bodyParser = require('body-parser');
const cors = require('cors');
const express = require("express");
const apiRouter = require('./routes');

require('dotenv').config();

const app = express();
app.use(cors({
    origin: process.env.WEB_CLIENT
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api', apiRouter);

app.listen(process.env.SERVER_PORT, ()=>{
    console.log('server listening port '+process.env.SERVER_PORT);
})